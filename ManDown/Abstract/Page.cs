﻿using System;
namespace ManDown
{
	public abstract class Page
	{
		public string OriginalName;

		public abstract byte[] GetBytes();
	}
}
