﻿using System;
using System.Collections.Generic;
namespace ManDown
{
	public abstract class Scanner
	{
		public abstract List<Manga> GetPopularMangas(int num);

		public abstract List<Manga> Query(string q);
	}
}
