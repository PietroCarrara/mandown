﻿using System;
using System.Collections.Generic;

namespace ManDown
{
	public abstract class Chapter
	{
		public string Title;

		public int Number;

		public abstract List<Page> GetPages();
	}
}
