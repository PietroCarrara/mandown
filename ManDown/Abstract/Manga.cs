﻿using System;
using System.Collections.Generic;

namespace ManDown
{
	public abstract class Manga
	{
		public string Title;

		public abstract List<Chapter> GetChapters();
	}
}
