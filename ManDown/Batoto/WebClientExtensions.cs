﻿using System;
using System.Net;
namespace ManDown.Batoto
{
	public static class WebClientExtensions
	{
		public static void SetUser(this WebClient self, BatotoUser u)
		{
			self.Headers.Add(HttpRequestHeader.Cookie,
						   "pass_hash=" + u.PassHash + "; " +
						   "member_id=" + u.MemberID);
		}
	}
}
