﻿using System;
using AngleSharp.Dom;
using System.Net;
using AngleSharp.Parser.Html;

namespace ManDown.Batoto
{
	class BatotoPage : Page
	{
		private string link;

		public BatotoPage(string e, BatotoUser u)
		{
			var wc = new WebClient();

			wc.Headers.Add(HttpRequestHeader.Referer,
						   "https://bato.to/reader");

			wc.SetUser(u);

			// Straight copy/paste from bato.to
			var split = e.Substring(e.IndexOf('#') + 1).Split('_');

			var id = "";
			var page = "1";
			var suppress = "";

			if (split.Length >= 2)
			{
				page = split[1];
				if (split.Length == 3)
				{
					suppress = "&supress_webtoon=" + split[2];
				}
			}
			id = split[0];

			var html = wc.DownloadString(BatotoScanner.URL + "areader?id=" + id + "&p=" + page + suppress);

			var doc = new HtmlParser().Parse(html);

			link = doc.QuerySelector("img#comic_page").GetAttribute("src");
			this.OriginalName = link.Substring(link.LastIndexOf('/') + 1);
		}

		public override byte[] GetBytes()
		{
			var wc = new WebClient();

			try
			{
				return wc.DownloadData(link);
			}
			catch (Exception e)
			{
				Console.Write(" Failed with error: " + e.Message + ". Retrying... ");

				return GetBytes();
			}
		}
	}
}