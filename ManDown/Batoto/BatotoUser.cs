﻿using System;
using System.Net;
using System.Collections.Specialized;
using AngleSharp.Parser.Html;
using System.Collections.Generic;

namespace ManDown.Batoto
{
	public class BatotoUser
	{
		private string username, password;

		private string passHash;
		public string PassHash
		{
			get
			{
				return passHash;
			}
		}

		private string memberID;
		public string MemberID
		{
			get
			{
				return memberID;
			}
		}

		private bool logged = false;
		public bool Logged
		{
			get
			{
				return logged;
			}
		}

		public BatotoUser(string username, string password)
		{
			this.username = username;
			this.password = password;
		}

		public void Login()
		{
			var wc = new BatotoWebClient();

			var reqparm = new NameValueCollection();

			reqparm.Add("auth_key", getAuthCode(wc));
			reqparm.Add("rememberMe", "1");
			reqparm.Add("ips_username", username);
			reqparm.Add("ips_password", password);

			wc.UploadValues("https://bato.to/forums/index.php?app=core&module=global&section=login&do=process", reqparm);

			passHash = wc.GetPassHash();
			memberID = wc.GetMemberID();
		}

		private string getAuthCode(WebClient wc)
		{
			var html = wc.DownloadString(BatotoScanner.URL);
			var parser = new HtmlParser();
			var doc = parser.Parse(html);

			var authInput = doc.QuerySelector("form#login input");

			return authInput.GetAttribute("value");
		}
	}

	// Small class to get response cookies
	class BatotoWebClient : WebClient
	{
		private readonly CookieContainer cookies = new CookieContainer();

		protected override WebRequest GetWebRequest(Uri address)
		{
			WebRequest request = base.GetWebRequest(address);
			HttpWebRequest webRequest = request as HttpWebRequest;
			if (webRequest != null)
			{
				webRequest.CookieContainer = cookies;
			}

			return request;
		}

		public string GetPassHash()
		{
			return cookies.GetCookies(BatotoScanner.BaseUri)["pass_hash"].Value;
		}

		public string GetMemberID()
		{
			return cookies.GetCookies(BatotoScanner.BaseUri)["member_id"].Value;
		}
	}
}
