﻿using System;
using System.Collections.Generic;
using AngleSharp.Dom;
using System.Net;
using AngleSharp.Parser.Html;
using System.Linq;

namespace ManDown.Batoto
{
	class BatotoManga : Manga
	{
		private string link;

		private BatotoUser user;

		public BatotoManga(IElement e, BatotoUser u)
		{
			var data = e.QuerySelector("strong a");

			Title = data.TextContent.Trim();

			link = data.GetAttribute("href");

			user = u;
		}

		public override List<Chapter> GetChapters()
		{
			var wc = new WebClient();

			wc.SetUser(user);

			var html = wc.DownloadString(link);

			return parseChapters(html);
		}

		private List<Chapter> parseChapters(string html)
		{
			var parser = new HtmlParser();

			var doc = parser.Parse(html);

			var chpts = doc.QuerySelectorAll("table.chapters_list tr.lang_English");

			var res = new List<Chapter>();
			foreach (var c in chpts)
			{
				res.Add(new BatotoChapter(c, user));
			}

			res.Reverse();

			killDuplicates(res);

			return res;
		}

		private void killDuplicates(List<Chapter> l)
		{
			var prev = l[0];

			for (int i = 1; i < l.Count; i++)
			{
				var prevName = prev.Title.Substring(0, prev.Title.IndexOf(':'));
				var currName = l[i].Title.Substring(0, l[i].Title.IndexOf(':'));

				if (prevName == currName)
				{
					l.Remove(l[i]);
					i--;
				}
				else
				{
					prev = l[i];
				}
			}

			int num = 1;
			foreach (var chapter in l)
			{
				chapter.Number = num++;
			}
		}
	}
}