﻿using System;
using System.Collections.Generic;
using AngleSharp.Dom;
using System.Net;
using AngleSharp.Parser.Html;

namespace ManDown.Batoto
{
	class BatotoChapter : Chapter
	{
		private string link;
		private BatotoUser user;

		public BatotoChapter(IElement e, BatotoUser u)
		{
			var data = e.QuerySelector("td a");

			Title = data.TextContent.Trim();

			link = data.GetAttribute("href");

			user = u;
		}

		public override List<Page> GetPages()
		{
			var wc = new WebClient();

			wc.Headers.Add(HttpRequestHeader.Referer,
						   "https://bato.to/reader");

			wc.SetUser(user);

			// Straight copy/paste from bato.to
			var split = link.Substring(link.IndexOf('#') + 1).Split('_');

			var id = "";
			var page = "1";
			var suppress = "";

			if (split.Length >= 2)
			{
				page = split[1];
				if (split.Length == 3)
				{
					suppress = "&supress_webtoon=" + split[2];
				}
			}
			id = split[0];

			var html = wc.DownloadString(BatotoScanner.URL + "areader?id=" + id + "&p=" + page + suppress);

			return parsePages(html);
		}

		List<Page> parsePages(string html)
		{
			var parser = new HtmlParser();

			var doc = parser.Parse(html);

			var slct = doc.QuerySelector("select#page_select");
			var pages = slct.QuerySelectorAll("option");

			var res = new List<Page>();
			foreach (var p in pages)
			{
				res.Add(new BatotoPage(p.GetAttribute("value"), user));
			}

			return res;
		}
	}
}