﻿using System;
using System.Net;
using AngleSharp.Parser.Html;
using System.Collections.Generic;

namespace ManDown.Batoto
{
	public class BatotoScanner : Scanner
	{
		public const string URL = "https://bato.to/";

		public static Uri BaseUri
		{
			get
			{
				return new Uri(URL);
			}
		}

		private BatotoUser user;

		public BatotoScanner(string username, string password)
		{
			user = new BatotoUser(username, password);
		}

		public override List<Manga> GetPopularMangas(int num)
		{
			if (!user.Logged)
				user.Login();

			var wc = new WebClient();

			var res = new List<Manga>();

			for (int i = 0; i < num % 30; i++)
			{
				var html = wc.DownloadString(URL + "search?order_cond=views&order=desc");

				res.AddRange(parseMangas(html));
			}

			return res.GetRange(0, num);
		}

		public override List<Manga> Query(string q)
		{
			if (!user.Logged)
				user.Login();

			var wc = new WebClient();

			var html = wc.DownloadString(URL + "search?name=" + q + "&name_cond=c");

			return parseMangas(html);
		}

		private List<Manga> parseMangas(string html)
		{
			var parser = new HtmlParser();

			var doc = parser.Parse(html);

			var chpts = doc.QuerySelectorAll("table.chapters_list tr:nth-child(even):not(:last-child)");

			var res = new List<Manga>();
			foreach (var e in chpts)
			{
				res.Add(new BatotoManga(e, user));
			}

			return res;
		}
	}
}
