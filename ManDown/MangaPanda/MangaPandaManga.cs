﻿using System;
using System.Collections.Generic;
using AngleSharp.Dom;
using System.Net;
using AngleSharp.Parser.Html;

namespace ManDown.MangaPanda
{
	public class MangaPandaManga : Manga
	{
		private Uri uri;

		public MangaPandaManga(IElement e)
		{
			var title = e.QuerySelector(".manga_name h3 a");

			this.Title = title.TextContent;

			this.uri = new Uri(MangaPandaScanner.BaseUri, title.GetAttribute("href"));
		}

		public override List<Chapter> GetChapters()
		{
			var wc = new WebClient();

			var html = wc.DownloadString(uri);

			return parseChapters(html);
		}

		private List<Chapter> parseChapters(string html)
		{
			var parser = new HtmlParser();

			var doc = parser.Parse(html);

			// Select all tr except the first
			var chaps = doc.QuerySelectorAll("table#listing tr:not(:first-child)");

			var res = new List<Chapter>();

			int i = 1;
			foreach (var c in chaps)
				res.Add(new MangaPandaChapter(c, i++));

			return res;
		}
	}
}
