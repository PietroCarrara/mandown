﻿using System;
using System.Collections.Generic;
using System.Net;
using AngleSharp.Parser.Html;
using System.Linq;

namespace ManDown.MangaPanda
{
	public class MangaPandaScanner : Scanner
	{
		public const string URL = "http://www.mangapanda.com/";
		public static Uri BaseUri
		{
			get
			{
				return new Uri(URL);
			}
		}

		public override List<Manga> GetPopularMangas(int num)
		{
			var wc = new WebClient();

			// How many pages we have to run to get <num> mangas
			int numOfPages = num % 30;

			var res = new List<Manga>();
			for (int i = 0; i < numOfPages; i++)
			{
				var html = wc.DownloadString(URL + "popular/" + i * 30);

				res.AddRange(parseMangas(html));
			}

			return res.GetRange(0, num);
		}

		public override List<Manga> Query(string q)
		{
			string url = URL + "search/?rd=0&status=0&order=0&genre=0000000000000000000000000000000000000&p=0&w=";

			string query = url + WebUtility.UrlEncode(q);

			var wc = new WebClient();

			var html = wc.DownloadString(query);

			return parseMangas(html);
		}

		private List<Manga> parseMangas(string html)
		{
			var parser = new HtmlParser();

			var doc = parser.Parse(html);

			var elements = doc.QuerySelectorAll(".mangaresultitem");

			var res = new List<Manga>();

			foreach (var e in elements)
				res.Add(new MangaPandaManga(e));

			return res;
		}
	}
}
