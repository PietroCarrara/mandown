﻿using System;
using System.Collections.Generic;
using AngleSharp.Dom;
using System.Net;
using AngleSharp.Parser.Html;

namespace ManDown.MangaPanda
{
	public class MangaPandaChapter : Chapter
	{
		public Uri uri;

		public MangaPandaChapter(IElement e, int number)
		{
			this.uri = new Uri(MangaPandaScanner.BaseUri, e.QuerySelector("a").GetAttribute("href"));

			var uneeded = e.QuerySelector("a").TextContent;

			var title = e.QuerySelector("td").TextContent;
			title = title.Replace(uneeded + " : ", "");

			if (title == "")
				title = "Untitled";

			this.Title = title.Trim();

			this.Number = number;
		}

		public override List<Page> GetPages()
		{
			var wc = new WebClient();

			var html = wc.DownloadString(uri);

			return parsePages(html);
		}

		private List<Page> parsePages(string html)
		{
			var parser = new HtmlParser();

			var doc = parser.Parse(html);

			var pages = doc.QuerySelectorAll("select#pageMenu option");

			var res = new List<Page>();
			foreach (var page in pages)
			{
				var link = page.GetAttribute("value");
				var uri = new Uri(MangaPandaScanner.BaseUri, link);

				var p = new MangaPandaPage(uri);

				res.Add(p);
			}

			return res;
		}
	}
}
