﻿using System;
using System.Net;
using AngleSharp.Parser.Html;
namespace ManDown.MangaPanda
{
	public class MangaPandaPage : Page
	{
		private Uri uri;

		public MangaPandaPage(Uri uri)
		{
			this.uri = uri;
		}

		public override byte[] GetBytes()
		{
			var wc = new WebClient();

			var html = wc.DownloadString(uri);

			return parsePage(html, wc);
		}

		private byte[] parsePage(string html, WebClient downloader)
		{
			var parser = new HtmlParser();

			var doc = parser.Parse(html);

			var imgUrl = doc.QuerySelector("img#img").GetAttribute("src");

			var imgUri = new Uri(MangaPandaScanner.BaseUri, imgUrl);

			int i = imgUri.ToString().LastIndexOf('/') + 1;
			this.OriginalName = imgUri.ToString().Substring(i);

			return downloader.DownloadData(imgUri);
		}
	}
}
