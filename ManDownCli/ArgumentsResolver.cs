﻿using System;
using System.Collections.Generic;
namespace ManDownCli
{
	public class ArgumentsResolver
	{
		private Dictionary<string, string> vals = new Dictionary<string, string>();

		public ArgumentsResolver(string[] args)
		{
			// Default values
			vals.Add("start", "1");
			vals.Add("end", "-1");
			vals.Add("src", "mangapanda");
			vals.Add("dir", "./");
			vals.Add("q", "");
			vals.Add("usr", "");
			vals.Add("pwd", "");

			foreach (var arg in args)
			{
				var pair = arg.Split('=');

				// If the user typed in '<value>' instead of
				// '<key>=<value>', infer the <key> as the query
				if (pair.Length <= 1)
					PutString("q", pair[0]);
				else
					PutString(pair[0], pair[1]);
			}
		}

		public int GetInt(string key)
		{
			try
			{
				return int.Parse(vals[key]);
			}
			catch (KeyNotFoundException)
			{
				return -1;
			}
			catch (FormatException)
			{
				return -1;
			}
		}

		public string GetString(string key)
		{
			try
			{
				return vals[key];
			}
			catch (KeyNotFoundException)
			{
				return "";
			}
		}

		public void PutString(string key, string val)
		{
			if (vals.ContainsKey(key))
				vals[key] = val;
			else
				vals.Add(key, val);
		}

		public void PutInt(string key, int i)
		{
			var num = i.ToString();

			if (vals.ContainsKey(key))
				vals[key] = num;
			else
				vals.Add(key, num);
		}
	}
}
