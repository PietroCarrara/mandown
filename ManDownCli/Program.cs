﻿using System;
using ManDown.MangaPanda;
using System.IO;
using ManDown.Batoto;
using ManDown;

namespace ManDownCli
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			var ar = new ArgumentsResolver(args);

			Scanner s = null;

			switch (ar.GetString("src").ToLower())
			{
				case "batoto":
					s = new BatotoScanner(ar.GetString("usr"), ar.GetString("pwd"));
					break;
				case "mangapanda":
					s = new MangaPandaScanner();
					break;
			}

			var d = new Downloader(s, ar.GetString("q"));

			d.Dir = ar.GetString("dir");

			d.Start = ar.GetInt("start");
			d.End = ar.GetInt("end");

			d.Run();
		}
	}
}
