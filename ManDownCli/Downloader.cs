﻿using System;
using System.IO;
using ManDown;
using System.Collections.Generic;
namespace ManDownCli
{
	public class Downloader
	{
		public int Start = 1, End = -1;

		public Scanner Scanner;

		public string Dir = "./", Query;

		public Downloader(Scanner s, string q)
		{
			Scanner = s;
			Query = q;
		}

		public void Run()
		{
			Directory.SetCurrentDirectory(Dir);

			var m = select(Scanner.Query(Query));

			if (m == null)
			{
				Console.WriteLine("No manga was found using query \"" + Query + "\"");
				return;
			}

			Console.WriteLine("Downloading " + m.Title);

			Directory.CreateDirectory(m.Title);
			Directory.SetCurrentDirectory(m.Title);

			var chapters = m.GetChapters();

			if (End < 0 || End > chapters.Count)
				End = chapters.Count;

			for (int i = Start - 1; i < End; i++)
			{
				var name = chapters[i].Number + " - " + chapters[i].Title;

				Console.WriteLine("Downloading " + name + "...");

				Directory.CreateDirectory(name);
				Directory.SetCurrentDirectory(name);

				int j = 1;
				foreach (var p in chapters[i].GetPages())
				{
					Console.Write("\t" + j + "...");

					var data = p.GetBytes();

					var ext = p.OriginalName.Substring(p.OriginalName.LastIndexOf('.'));

					File.WriteAllBytes(j + ext, data);

					Console.WriteLine(" Ok!");

					j++;
				}

				Directory.SetCurrentDirectory("..");
			}
		}

		private Manga select(List<Manga> mangas)
		{
			int i = 1;
			foreach (Manga m in mangas)
			{
				Console.WriteLine(i + ":\t" + m.Title);
				i++;
			}

			if (mangas.Count > 0)
			{
				Console.WriteLine("Select a manga to download:");

				int opt = int.Parse(Console.ReadLine());

				return mangas[opt - 1];
			}

			return null;
		}
	}
}